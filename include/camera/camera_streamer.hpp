#ifndef ROBOT_STREAM_CAMERA_STREAM_HPP_
#define ROBOT_STREAM_CAMERA_STREAM_HPP_

#include "robot_stream/msg/float_message.hpp"
#include "robot_stream/srv/float_get_service.hpp"
#include "robot_stream/srv/float_set_service.hpp"
#include "robot_stream/utility/float_streamer.hpp"
#include "rclcpp/rclcpp.hpp"

namespace robot_stream
{
  namespace camera
  {
    class CameraStreamer
    {
    public:

      using FloatStreamer = robot_stream::utility::FloatStreamer;
      using ImageStreamer = robot_stream::utility::ImageStreamer;

      FloatStreamer gain_streamer;
      FloatStreamer brightness_streamer;
      FloatStreamer contrast_streamer;
      FloatStreamer saturation_streamer;
      FloatStreamer temperature_streamer;

      ImageStreamer image_streamer;
    };
  }
}

#endif