#ifndef ROBOT_STREAM_UTILITY_FLOAT_STREAMER_HPP_
#define ROBOT_STREAM_UTILITY_FLOAT_STREAMER_HPP_

#include "robot_stream/msg/float_message.hpp"
#include "robot_stream/srv/float_get_service.hpp"
#include "robot_stream/srv/float_set_service.hpp"
#include "rclcpp/rclcpp.hpp"

#include <memory>
#include <string>

namespace robot_stream
{
  namespace utility
  {
    class FloatStreamer
    {
    public:

      using FloatMessage = robot_stream::msg::FloatMessage;
      using FloatGetService = robot_stream::srv::FloatGetService;
      using FloatSetService = robot_stream::srv::FloatSetService;

      FloatStreamer(
        const std::shared_ptr<rclcpp::Node> &shared_rcl_node, const std::string &stream_name,
        const FloatMessage &message = FloatMessage()
      );

      void publishMessage(const FloatMessage &message);

    private:

      void feedbackSetMessage(
        const std::shared_ptr<FloatSetService::Request> shared_request,
        std::shared_ptr<FloatSetService::Response> shared_response
      );
      void feedbackGetMessage(
        const std::shared_ptr<FloatGetService::Request> shared_request,
        std::shared_ptr<FloatGetService::Response> shared_response
      );

      rclcpp::Node::SharedPtr shared_rcl_node;
      std::string stream_name;

      std::shared_ptr<rclcpp::Publisher<FloatMessage>> shared_rcl_publisher;
      std::shared_ptr<rclcpp::Service<FloatSetService>> shared_rcl_set_service;
      std::shared_ptr<rclcpp::Service<FloatGetService>> shared_rcl_get_service;

      FloatMessage message;
    };
  }
}

#endif