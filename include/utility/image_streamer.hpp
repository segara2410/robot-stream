#ifndef ROBOT_STREAM_UTILITY_IMAGE_STREAMER_HPP_
#define ROBOT_STREAM_UTILITY_IMAGE_STREAMER_HPP_

#include "robot_stream/msg/image_message.hpp"
#include "robot_stream/srv/image_get_service.hpp"
#include "robot_stream/srv/image_set_service.hpp"
#include "rclcpp/rclcpp.hpp"

#include <memory>
#include <string>

namespace robot_stream
{
  namespace utility
  {
    class ImageStreamer
    {
    public:

      using ImageMessage = robot_stream::msg::ImageMessage;
      using ImageGetService = robot_stream::srv::ImageGetService;
      using ImageSetService = robot_stream::srv::ImageSetService;

      ImageStreamer(
        const std::shared_ptr<rclcpp::Node> &shared_rcl_node, const std::string &stream_name,
        const ImageMessage &message = ImageMessage()
      );

      void publishMessage(const ImageMessage &message);

    private:

      void feedbackSetMessage(
        const std::shared_ptr<ImageSetService::Request> shared_request,
        std::shared_ptr<ImageSetService::Response> shared_response
      );
      void feedbackGetMessage(
        const std::shared_ptr<ImageGetService::Request> shared_request,
        std::shared_ptr<ImageGetService::Response> shared_response
      );

      rclcpp::Node::SharedPtr shared_rcl_node;
      std::string stream_name;

      std::shared_ptr<rclcpp::Publisher<ImageMessage>> shared_rcl_publisher;
      std::shared_ptr<rclcpp::Service<ImageSetService>> shared_rcl_set_service;
      std::shared_ptr<rclcpp::Service<ImageGetService>> shared_rcl_get_service;

      ImageMessage message;
    };
  }
}

#endif