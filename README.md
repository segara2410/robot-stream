# Robot Stream

## Utility
- Class FloatReceiver
  - Contains `FloatMessage message`
  - Contains `request_to_get_message()` function to request to get message using ros service.
  - Contains `request_to_set_message()` function to request to set message using ros service.
  - Contains `subscribe_message()` function to subscribe using ros subscriber.
- Class FloatStreamer
  - Contains `FloatMessage message`
  - Contains `feedback_get_message()` function to feedback get message using ros service.
  - Contains `feedback_set_message()` function to feedback set message using ros service.
  - Contains `publish_message()` function to publish requested message using ros publisher.
- Class ImageReceiver
  - Contains `ImageMessage message`.
  - Contains `request_to_get_message()` function to request to get message using ros service.
  - Contains `request_to_set_message()` function to request to set message using ros service.
  - Contains `subscribe_message()` function to subscribe using ros subscriber.
- Class ImageStreamer
  - Contains `ImageMessage message`.
  - Contains `feedback_get_message()` function to feedback get message using ros service.
  - Contains `feedback_set_message()` function to feedback set message using ros service.
  - Contains `publish_message()` function to publish requested message using ros publisher.
- Class Vector3Receiver
  - Contains `Vector3 message`
  - Contains `request_to_get_message()` function to request to get message using ros service.
  - Contains `request_to_set_message()` function to request to set message using ros service.
  - Contains `subscribe_message()` function to subscribe using ros subscriber.
- Class Vector3Streamer
  - Contains `Vector3 message`
  - Contains `feedback_get_message()` function to feedback get message using ros service.
  - Contains `feedback_set_message()` function to feedback set message using ros service.
  - Contains `publish_message()` function to publish requested message using ros publisher.

## Camera
- Class CameraReceiver
  - Contains `Mat image`, `float gain`, `float exposure`, `float brightness`, `float contrast`, `float saturation`, and `float temperature`.
  - Contains `ImageReceiver image_receiver`, `FloatReceiver gain_receiver`, `FloatReceiver exposure_receiver`, `FloatReceiver brightness_receiver`, `FloatReceiver contrast_receiver`, `FloatReceiver saturation_receiver`, and `FloatReceiver temperature_receiver`.
- Class CameraStreamer
  - Contains `Mat image`, `float gain`, `float exposure`, `float brightness`, `float contrast`, `float saturation`, and `float temperature`.
  - Contains `ImageStreamer image_streamer`, `FloatStreamer gain_streamer`, `FloatStreamer exposure_streamer`, `FloatStreamer brightness_streamer`, `FloatStreamer contrast_streamer`, `FloatStreamer saturation_streamer`, and `FloatStreamer temperature_streamer`.

## IMU
- Class ImuReceiver
  - Contains `Vector3 accelerometer_values` and `Vector3 gyroscope_values`
  - Contains `Vector3Receiver accelerometer_values_receiver` and `Vector3Receiver gyroscope_values_receiver`
- Class ImuStreamer
  - Contains `Vector3 accelerometer_values` and `Vector3 gyroscope_values`
  - Contains `Vector3Streamer accelerometer_values_streamer` and `Vector3Streamer gyroscope_values_streamer`

## Joint
- Class BodyJointsReceiver
  - Contains `BodyJoints message`
  - Contains `request_to_get_message()` function to request to get message using ros service.
  - Contains `request_to_set_message()` function to request to set message using ros service.
  - Contains `subscribe_message()` function to subscribe using ros subscriber.
- Class BodyJointsStreamer
  - Contains `BodyJoints message`
  - Contains `feedback_get_message()` function to feedback get message using ros service.
  - Contains `feedback_set_message()` function to feedback set message using ros service.
  - Contains `publish_message()` function to publish requested message using ros publisher.